<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => '/api/v1/product'], function ($router) { 
    $router->post('/create','ProductController@create');
    $router->get('/lists','ProductController@index');
    $router->get('/detail/{id}','ProductController@show');
    $router->put('/update/{id}','ProductController@update');
    $router->delete('/delete/{id}','ProductController@destroy');
});

$router->group(['prefix' => '/api/v1/cart'], function ($router) { 
    $router->post('/add_to_cart','CartController@cart');
    $router->get('/list_cart/{id}','CartController@index');
    $router->get('/detail/{id}','CartController@show');
    $router->put('/update_cart/{id}','CartController@update');
    $router->delete('/delete_cart/{id}','CartController@destroy');
    $router->post('/checkout','CartController@checkout');

});

$router->group(['prefix' => '/api/v1/auth'], function ($router) { 
    $router->post('/register','UserController@register');
    $router->post('/login','UserController@login');
    $router->post('/logout','UserController@logout');
});

