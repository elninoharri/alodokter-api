<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'refresh', 'logout', 'register']]);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "email" => "required|unique:users|email",
            "password" => "required"
        ]);

        if ($validator->fails()) {
            $errorString = implode(",",$validator->messages()->all());
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => $errorString
            ], 400);
        }

        $email = $request->input('email');
        $password = Hash::make($request->input('password'));
        
        try {
            $user = User::create([
                'email' => $email,
                'password' => $password
            ]);
    
            return response()->json([
                'status' => env('STATUS_SUCCESS_TEXT'),
                'message' => 'Successfully registration'
            ], 201);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => 'Error while registration'
            ], 400);
        }
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "email" => "required|email",
            "password" => "required|min:6"
        ]);

        $credentials = $request->only(['email', 'password']);

        if (!$token = Auth::attempt($credentials)) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }
        
        if ($validator->fails()) {
            $errorString = implode(",",$validator->messages()->all());
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => $errorString
            ], 400);
        }

        $email = $request->input('email');
        $password = $request->input('password');
  
        $user = User::where('email', $email)->first();
        
        if (!$user) {
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => 'Login failed, invalid username or password ...'
            ], 401);
        }
  
        $isValidPassword = Hash::check($password, $user->password);
        if (!$isValidPassword) {
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => 'Login failed, invalid username or password ...'
            ], 401);
        }
  
        $generateToken = bin2hex(random_bytes(40));
        
        try {
            $user->update([
                'token' => $generateToken
            ]);

            return response()->json([
                'status' => env('STATUS_SUCCESS_TEXT'),
                'data' => $this->respondWithToken($token)
            ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => 'Error While Login'
            ], 401);
        }
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'user' => auth()->user(),
            'expires_in' => auth()->factory()->getTTL() * 60 * 24
        ]);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }
} 
